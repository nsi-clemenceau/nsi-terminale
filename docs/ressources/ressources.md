---
author: Mireille Coilhac
title: Ressources
---

## Lien vers le site de première

[NSI 1ère](https://mcoilhac.forge.aeif.fr/site-nsi/){ .md-button target="_blank" rel="noopener" }


## Entraînement à l'épreuve pratique : 

[CodEx le codage par les exercices](https://codex.forge.apps.education.fr/){ .md-button target="_blank" rel="noopener" }

[Ancien site BAC pratique en cas de panne de CodEx](https://e-nsi.forge.aeif.fr/pratique/){ .md-button target="_blank" rel="noopener" }

[Exercices supplémentaires](https://mcoilhac.forge.aeif.fr/term/exercices_pratiques/chiffres_romains/sujet/){ .md-button target="_blank" rel="noopener" }




## Lien pour réviser les bases de données et SQL

[SQL par Nicolas Revéret](https://e-nsi.forge.aeif.fr/exercices_bdd/){ .md-button target="_blank" rel="noopener" }

Pour la page suivante, il n'est pas nécessaire de s'inscrire :  
[Colibri](https://colibri.unistra.fr/fr/course/list/notions-de-base-en-sql){ .md-button target="_blank" rel="noopener" }


## lien pour approfondir les tris : utile pour préparer l'écrit du bac

Suivre le lien suivant.

Faire tout à partir de ce niveau du chapitre. En cas de besoin, faire les paragraphes précédants.

[Tris - Nicolas Revéret](https://e-nsi.forge.aeif.fr/tris/02_complexite/01_recherches/){ .md-button target="_blank" rel="noopener" }

## Lien pour approfondir la récursivité

[Récursivité - Franck Chambon](https://e-nsi.forge.aeif.fr/recursif/){ .md-button target="_blank" rel="noopener" }


## Lien vers le site d'entraînement à l'épreuve écrite : 

[BAC écrit e-nsi](https://e-nsi.forge.aeif.fr/ecrit/){ .md-button target="_blank" rel="noopener" }

## Lien vers les annales du site de Fabrice Nativel : 

[BAC écrit  F. Nativel](https://fabricenativel.github.io/index_annales/){ .md-button target="_blank" rel="noopener" }


## Lien vers des ressources Interstices

[interstices.info](https://interstices.info/?s=&fwp_type=animation){ .md-button target="_blank" rel="noopener" }


## Lien vers le site de sujets du bac : ToutMonExam

[ToutMonExam](https://toutmonexam.fr/){ .md-button target="_blank" rel="noopener" }


## Lien pour le postbac : orientation et études

En bas de cette page, vous trouverez également des MOOCs pour réviser les maths ou la physique en vue des études supérieures.

[MOOC orientation révisions](https://www.mooc-orientation.fr/){ .md-button target="_blank" rel="noopener" }

## Liens sur des sujets divers

[Pi](https://fchambon.forge.aeif.fr/piday/){ .md-button target="_blank" rel="noopener" }

### Les nombres réels, des suites, etc.

 Vous devez travailler sur [Basthon](https://notebook.basthon.fr/){ .md-button target="_blank" rel="noopener" }

🌐 TD `les_nombres_et_python.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/les_nombres_et_python.ipynb)

### Des maths

[Maths cnrs](https://images.math.cnrs.fr/){ .md-button target="_blank" rel="noopener" }

Et particulièrement : regarder la rubrique Dossiers


[Mysteres de la suite de Fibonacci](https://images.math.cnrs.fr/Mysteres-arithmetiques-de-la-suite-de-Fibonacci?lang=es&id_forum=11953){ .md-button target="_blank" rel="noopener" }

[Vidéos Maths cnrs](https://video.math.cnrs.fr/category/selection-de-videos/){ .md-button target="_blank" rel="noopener" }

[Nombre d'or](https://images.math.cnrs.fr/Le-Nombre-d-or.html?lang=es&id_forum=6702){ .md-button target="_blank" rel="noopener" }



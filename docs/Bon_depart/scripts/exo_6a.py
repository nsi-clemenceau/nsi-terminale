def indiscernables(nombre1, nombre2):
    return abs(nombre1 - nombre2) < 1e-15

def moyenne_ponderee(valeurs):
    ...

# tests
assert indiscernables(moyenne_ponderee([(5, 1), (15, 1)]), 10.0)
assert indiscernables(moyenne_ponderee([(5, 1), (15, 2)]), 11.666666666666666)
assert indiscernables(moyenne_ponderee([(5, 1), (15, 3)]), 12.5)
assert indiscernables(moyenne_ponderee([(5, 1), (15, 3), (20, 0)]), 12.5)

def dichotomie(nombres, cible):
    ...

# Tests
assert dichotomie([1, 2, 3, 4], 2) == True
assert dichotomie([1, 2, 3, 4], 1) == True
assert dichotomie([1, 2, 3, 4], 4) == True
assert dichotomie([1, 2, 3, 4], 5) == False
assert dichotomie([1, 2, 3, 4], 0) == False
assert dichotomie([1, 2, 5, 6], 4) == False
assert dichotomie([1], 1) == True
assert dichotomie([1], 0) == False
assert dichotomie([], 1) == False

# tests
assert indice_egal_valeur([7, 1, 8])
assert indice_egal_valeur([9, -7, 2, 9, 6])
assert not indice_egal_valeur([1, 2, 3, 4])
# tests secrets
assert indice_egal_valeur([7, 1, 8]) is True
assert indice_egal_valeur([9, -7, 2, 9, 6]) is True
assert indice_egal_valeur([1, 2, 3, 4]) is False

assert indice_egal_valeur([]) is False
assert indice_egal_valeur([0]) is True
assert indice_egal_valeur([1]) is False

assert indice_egal_valeur([1,0]) is False
assert indice_egal_valeur([20, 20, 2, 20, 20]) is True
assert indice_egal_valeur([9, 8, 7, 6, 5, 4, 3, 2, 1, 0]) is False
assert indice_egal_valeur([9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 10]) is True
assert indice_egal_valeur([10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0]) is True

# tests
assert effectifs([4, 1, 2, 4, 2, 2, 6]) == {4: 2, 1: 1, 2: 3, 6: 1}
assert effectifs(["chien", "chat", "chien", "chien", "poisson", "chat"]) == {'chien': 3, 'chat': 2, 'poisson': 1}
# tests secrets
assert effectifs([]) == {}
assert effectifs([0]) == {0: 1}
assert effectifs([0]*1000) == {0: 1000}
assert effectifs([0]*1000 + [1]*100 + [2]*10 + [3]) == {0: 1000, 1: 100, 2: 10, 3: 1}
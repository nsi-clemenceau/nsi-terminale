def compter_triples(entiers):
    cpt = 0
    for nombre in entiers:
        if nombre % 3 == 0:
            cpt = cpt + 1
    return cpt


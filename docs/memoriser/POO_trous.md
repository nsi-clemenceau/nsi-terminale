---
author: Mireille Coilhac
title: POO - À vous
---

!!! info "Le paradigme objet "

	Le paradigme objet est une autre façon de voir la programmation qui consiste à utiliser un structure de donnée appelée objet qui réunit des données et des fonctionnalités. Les données sont appelées $\hspace{7em}$ et les fonctionnalités sont appelées $\hspace{7em}$.
	
!!! info "Une classe "

	Une classe permet de définir un ensemble d’objets qui ont des caractéristiques communes. C’est un moule permettant de créer une infinité d’objets différents dans leurs valeurs mais similaires dans leur structure.

!!! info "Le mot clé `class` et les instances de classes"

	En Python, on utilise le mot clé $\hspace{6em}$ pour définir une classe qui devient alors un nouveau type abstrait de données. 

	On peut alors créer de nouveaux objets en appelant le constructeur qui porte le nom de la classe. Les objets ainsi créés s'appellent des $\hspace{7em}$ de la classe.

!!! info "La méthode `__init__` "

	 En Python, la méthode `__init__` est le constructeur de la classe. Elle est utilisée à la création des objets de la classe et initialise les valeurs des $\hspace{7em}$ de l’objet.

!!! info "La notation pointée"

	Les attributs et méthodes d'une instance de classe sont accessibles en utilisant la notation pointée : 

	* `objet.attribut`
	* `objet.methode(arguments)`
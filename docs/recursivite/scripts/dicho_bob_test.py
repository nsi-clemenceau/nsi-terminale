# Tests
assert recherche([1, 2, 3, 4, 5, 6], 1) is True
assert recherche([1, 2, 3, 4, 5, 6], 6) is True
assert recherche([1, 2, 3, 4, 5, 6], 8) is False
assert recherche([1, 2, 3, 4, 5, 6], 0) is False
assert recherche([1, 2, 3, 4, 5, 8], 7) is False


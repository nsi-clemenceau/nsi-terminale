def toto(n):
    """Une fonction telle que :

    - `toto(0)` renvoie `'0'`
    - `toto(1)` renvoie `'(0 + 0)'`
    - `toto(2)` renvoie `'((0 + 0) + (0 + 0))'`
    - etc sur le même principe

    n est un entier positif
    """

    if n == 0:
        return '0'
    else:
        motif = toto(n - 1)
        return '(' + motif + ' + ' + motif + ')'




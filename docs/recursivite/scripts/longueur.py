def longueur(tableau: list) -> int:
    """
    La fonction renvoie la longueur de tableau
    """
    ...



# Tests
assert longueur([]) == 0
assert longueur([1]) == 1
assert longueur([1, 2]) == 2
assert longueur([1, 2, 3]) == 3


def dico_lettres(mot):
    ...

def BMH(texte, motif):
    dico = ...
    indices = []
    i = ... # i sert à parcourir texte avec des sauts. On part du bout du motif.

    while i <= ...:
        k = 0
        while k < ... and motif[...] == texte[...]: 
            k = ...
        if k == ... :   # On a trouvé le motif
            indices.append(...)
            decalage = ...
            i = i + decalage  # On cherche un autre motif à droite
        else:
            if texte[i - k] in dico:
                decalage = ...
                i = max(... , i + 1)  # Il ne faut pas revenir en arrière
            else:
                decalage = ...
                i = ...
                
    return indices

# Tests
assert dico_lettres("string") == {"s": 0, "t": 1, "r": 2, "i": 3, "n": 4}
assert BMH("stupid spring string", "string") == [14]
assert BMH("stupid spring string", "ing") == [10, 17]
assert BMH("stupid spring string", "ok") == []

---
author: David Landry
title: 👏 Crédits
---

Auteurs des rubriques de ce site :  
Gilles Lassus, Fabrice Nativel, Nicolas Revéret, Jean-Louis Thirot, Mireille Coilhac et David Landry

Le site est hébergé par  [la forge des communs numériques éducatifs](https://docs.forge.apps.education.fr/){:target="_blank" }.

Le site est construit avec [`mkdocs`](https://www.mkdocs.org/) et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/).

Un grand merci à M. Vincent-Xavier Jumel et Vincent Bouillot qui ont réalisé la partie technique de ce site.

Le logo :material-lightbulb-group: fait partie de mkdocs sous la référence lightbulb-group

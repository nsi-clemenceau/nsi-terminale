# Tests
assert adresse1.adresse == '192.168.0.1'
assert adresse2.adresse == '192.168.0.2'
assert adresse3.adresse == '192.168.0.0'

assert adresse1.liste_octets() == [192, 168, 0, 1]
assert adresse1.est_reservee() is False
assert adresse3.est_reservee() is True
assert adresse2.adresse_suivante().adresse  == '192.168.0.3'

# Autres tests
adresse4 = AdresseIP('192.168.0.10')
assert adresse4.adresse_suivante().adresse  == '192.168.0.11'
assert adresse4.liste_octets() == [192, 168, 0, 10]
assert adresse4.est_reservee() is False

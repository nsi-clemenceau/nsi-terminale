# Tests
eleves_nsi = ['a','b','c','d','e','f','g','h','i','j']
notes_nsi = [30, 40, 80, 60, 58, 80, 75, 80, 60, 24]
assert pantheon(eleves_nsi, notes_nsi) == (80, ['c', 'f', 'h'])
assert pantheon([],[]) == (0, [])

# Autres tests
assert pantheon(eleves_nsi, 10*[0]) == (0, ['a','b','c','d','e','f','g','h','i','j'])
assert pantheon(eleves_nsi, [100, 40, 80, 60, 58, 80, 75, 80, 60, 100]) == (100, ['a', 'j'])
assert pantheon(eleves_nsi, 10*[20]) == (20, ['a','b','c','d','e','f','g','h','i','j'])
assert pantheon(eleves_nsi, [i for i in range(10)]) == (9, ['j'])
assert pantheon(eleves_nsi, [i for i in range(10, 0, -1)]) == (10, ['a'])


---
author: Gilles Lassus puis Mireille Coilhac
title: taille récursive d'un arbre binaire (1)
tags:
    - dictionnaire
    - arbre binaire
    - important
---

Dans cet exercice, un arbre binaire de caractères est stocké sous la forme d’un
dictionnaire où les clefs sont les caractères des nœuds de l’arbre et les valeurs, pour
chaque clef, la liste des caractères des fils gauche et droit du nœud.

On utilise la valeur `''` pour représenter un fils vide.

Par exemple, l’arbre

![image](arbre_1_2024.png){: .center}

est stocké dans

```python
a = {'F':['B','G'], 'B':['A','D'], 'A':['',''], 'D':['C','E'], \
'C':['',''], 'E':['',''], 'G':['','I'], 'I':['','H'], \
'H':['','']}
```

Écrire une fonction récursive `taille` prenant en paramètres un arbre binaire `arbre` non vide
représenté par un dictionnaire comme vu ci-dessus et un caractère `lettre`. La fonction doit renvoyer la taille 
de l'arbre, ou du sous arbre de `arbre` de sommet `lettre`. 

On observe que, par exemple, `arbre[lettre][0]`, respectivement
`arbre[lettre][1]`, permet d’atteindre la clé du sous-arbre gauche, respectivement
droit, de l’arbre `arbre` de sommet `lettre`.

!!! example "Exemple" 

    ```pycon
    >>> taille(a, 'F')
    9
    >>> taille(a, 'B')
    5
    >>> taille(a, 'I')
    2
    ```

???+ question "Compléter le code ci-dessous"

    {{ IDE('exo') }}
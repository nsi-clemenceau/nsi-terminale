# Tests
assert liste_puissances(3, 5) == [3, 9, 27, 81, 243]
assert liste_puissances(-2, 4) == [-2, 4, -8, 16]
assert liste_puissances_borne(2, 16) == [2, 4, 8]
assert liste_puissances_borne(2, 17) == [2, 4, 8, 16]
assert liste_puissances_borne(5, 5) == []

# Autres tests
assert liste_puissances(-2, 10) == [-2, 4, -8, 16, -32, 64, -128, 256, -512, 1024]
assert liste_puissances_borne(-2, 1000) == [-2, 4, -8, 16, -32, 64, -128, 256, -512]
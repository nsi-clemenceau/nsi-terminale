# Tests
assert traduire_romain("XIV") == 14
assert  traduire_romain("CXLII") == 142
assert traduire_romain("MMXXIII") == 2023

# Autres tests
assert traduire_romain("MCMXCI") == 1991
assert traduire_romain("MCMXCIX") == 1999
assert traduire_romain("MMMCMXCIX") == 3999
assert traduire_romain("XCIV") == 94
assert traduire_romain("MDCLXVI") == 1666
for cle in romains:
    assert traduire_romain(cle) == romains[cle]


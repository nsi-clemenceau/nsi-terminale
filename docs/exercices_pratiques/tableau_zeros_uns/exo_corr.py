def separe(zeros_et_uns):
    """place tous les 0 de zeros_et_uns à gauche et tous les 1 à droite"""
    debut = 0  # indice de début
    fin = len(zeros_et_uns) - 1    # indice de fin
    while debut < fin:
        if zeros_et_uns[debut] == 0:
            debut = debut + 1
        else:
            valeur = zeros_et_uns[fin]
            zeros_et_uns[fin] = zeros_et_uns[debut]
            zeros_et_uns[debut] = valeur
            fin = fin - 1

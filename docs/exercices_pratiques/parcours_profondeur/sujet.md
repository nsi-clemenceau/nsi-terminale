---
author: Gilles Lassus puis Mireille Coilhac
title: Parcours en profondeur de graphe
tags:
  - graphe
  - important
---

Dans cet exercice, on considère un graphe non orienté représenté sous forme de listes
d’adjacence. On suppose que les sommets sont numérotés de 0 à n-1.

Ainsi, le graphe suivant:

![image du graphe 2024 sujet 21](graphe_2024_21.png){: .center}


sera représenté par la liste d’adjacence suivante:

`adj = [[1, 2], [0, 3], [0], [1], [5], [4]]`

On souhaite déterminer les sommets accessibles depuis un sommet donné dans le graphe.
Pour cela, on va procéder à un parcours en profondeur du graphe.

Compléter les fonctions `parcours` et `accessibles`.

* La fonction `parcours` prend en paramètres une liste d'adjacence `liste_adjacence`, un entier `x` représentant un sommet, et la liste des sommets accessibles `sommets_accessibles`
Elle réalise un parcours en profondeur **récursif** du graphe donné par les listes 
d'adjacence `adjacence` depuis le sommet `x` en accumulant les sommets rencontrés dans la liste
`sommets_accessibles`

* La fonction `accessibles` prend en paramètres une liste d'adjacence `liste_adjacence`, un entier `x` représentant un sommet.  
Elle renvoie la liste des sommets accessibles dans le graphe donné par la listes d'adjacence `liste_adjacence` depuis le sommet `x`


!!! example "Exemples"

    !!! bug "Bug"

        Attention, les exemples donnés correspondent à un graphe orienté qui n'est pas celui représenté dans le sujet.

        ![graphe orienté](graphe_exemple_21_2.jpg){ width=30% }


    ```pycon
    >>> accessibles([[1, 2], [0], [0, 3], [1], [5], [4]], 0)
    [0, 1, 2, 3]
    >>> accessibles([[1, 2], [0], [0, 3], [1], [5], [4]], 4)
    [4, 5]
    ```

???+ question "Compléter le code ci-dessous"

    {{ IDE('exo') }}


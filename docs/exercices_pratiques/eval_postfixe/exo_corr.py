def evaluation_postfixe(expression):
    pile = Pile()
    for element in expression:
        if element != '+' and element != '*':
            pile.empiler(element)
        else:
            if element == '+':
                resultat = pile.depiler() + pile.depiler()
            else:
                resultat = pile.depiler() * pile.depiler()
            pile.empiler(resultat)
    return pile.depiler()
    
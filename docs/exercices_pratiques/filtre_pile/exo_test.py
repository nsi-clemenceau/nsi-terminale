# Tests
assert renverse([1, 2, 3, 4, 5]) == [5, 4, 3, 2, 1]
assert positifs([-1, 0, 5, -3, 4, -6, 10, 9, -8]) == [0, 5, 4, 10, 9]
assert positifs([-2]) == []

# Autres tests
assert renverse([]) == []
assert renverse([1]) == [1]
assert positifs([]) == []
assert positifs([1, -1, -1, -1]) == [1]
assert positifs([-1, -1, -1, 1]) == [1]


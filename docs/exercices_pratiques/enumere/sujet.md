---
author: Gilles Lassus puis Mireille Coilhac
title: Enumération de liste
tags:
  - liste/tableau
  - dictionnaire
  - Difficulté **
  - important
---

Écrire une fonction `enumere` qui prend en paramètre une liste `donnees` et renvoie un
dictionnaire dont les clés sont les éléments de `donnees` avec pour valeur associée la liste des
indices de l’élément dans la liste `donnees`.

!!! example "Exemple"

	```python
	>>> enumere([1, 1, 2, 3, 2, 1])
	{1: [0, 1, 5], 2: [2, 4], 3: [3]}
	```

???+ question "Compléter le code ci-dessous" 

    {{ IDE('exo') }}

#--- HDR ---#
from random import randint


class Tresor:
    """
    Classe mettant en oeuvre un trésor de 128 pièces d'or
    contenant une fausse pièce
    """

    def __init__(self):
        self.n_fausse = randint(1, 128)
        self.vraie_masse = 5
        self.fausse_masse = 4

    def __str__(self):
        """Renvoie la description du trésor"""
        return "Un trésor de 128 pièces"

    def __repr__(self):
        """Renvoie la description du trésor"""
        return self.__str__()


def compare(tresor, debut_gauche, fin_gauche, debut_droite, fin_droite):
    """
    Compare les masses des deux sous-trésors placés sur les plateaux
    de gauche et de droite.

    Le plateau de gauche contient les pièces du trésor dont les numéros
    de début et de fin respectifs sont
    debut_gauche (inclus) et fin_gauche (exclu)

    Le plateau de droite contient les pièces du trésor dont les numéros
    de début et de fin respectifs sont
    debut_droite (inclus) et fin_droite (exclu)

    Cette fonction renvoie le côté de la balance le plus léger :
    * la fonction renvoie "gauche" si le plateau de gauche est plus léger ;
    * la fonction renvoie "équilibre" si les deux plateaux ont la même masse ;
    * la fonction renvoie "droite" si le plateau de droite est plus léger.

    compare(trésor, 1, 15, 20, 50) compare donc les masses
    des pièces de numéros :
    * allant de 1 (inclus) à 15 exclu pour le sous-trésors de gauche
    * allant de 20 (inclus) à 50 exclu pour lesous-trésors de droite
    """
    if debut_gauche <= tresor.n_fausse < fin_gauche:
        masse_gauche = (
            tresor.vraie_masse * (fin_gauche - debut_gauche - 1) + tresor.fausse_masse
        )
    else:
        masse_gauche = tresor.vraie_masse * (fin_gauche - debut_gauche)

    if debut_droite <= tresor.n_fausse < fin_droite:
        masse_droite = (
            tresor.vraie_masse * (fin_droite - debut_droite - 1) + tresor.fausse_masse
        )
    else:
        masse_droite = tresor.vraie_masse * (fin_droite - debut_droite)

    if masse_gauche < masse_droite:
        return "gauche"
    elif masse_gauche == masse_droite:
        return "équilibre"
    else:
        return "droite"


#--- HDR ---#


def fausse_piece(tresor, debut, fin):
    """
    Renvoie le numéro de la fausse pièce dans le trésor
    Le trésor initial compte 128 pièces
    """
    ...


#  Tests
for i in range(10): # 10 essais
    tresor = Tresor()
    assert fausse_piece(tresor, 1, 129) == tresor.n_fausse


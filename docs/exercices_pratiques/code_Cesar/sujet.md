---
author: BNS2022-11.2, puis Romain Janvier et Mireille Coilhac
title: Code de César
tags:
  - 2-string
  - 4-modulo
---

# Le code de César

Le codage de César transforme un message en changeant chaque lettre par une autre obtenue par décalage dans l'alphabet de la lettre d'origine. Par exemple, avec un décalage de 3, le `'A'` se transforme en `'D'`, le `'B'` en `'E'`, ..., le `'X'` en `'A'`, le `'Y'` en `'B'` et le `'Z'` en `'C'`. Les autres caractères (`'!'`, `'?'`...) ne sont pas codés et sont simplement recopiés tels quels dans le message codé. 

La fonction `position_alphabet` prend en paramètre un caractère `lettre` et renvoie la position de `lettre` dans la chaine de caractères `ALPHABET` s'il s'y trouve.

Ecrire la fonction `cesar` qui prend en paramètres une chaîne de caractères `message` et un nombre entier `decalage` et renvoie le nouveau message codé avec le codage de César utilisant ce `decalage`.

!!! example "Exemples"

 
    ```pycon
    >>> cesar('BONJOUR A TOUS. VIVE LA MATIERE NSI !', 4)
    'FSRNSYV E XSYW. ZMZI PE QEXMIVI RWM !'
    >>> cesar('GTSOTZW F YTZX. ANAJ QF RFYNJWJ SXN !', -5)
    'BONJOUR A TOUS. VIVE LA MATIERE NSI !'
    ```  

??? tip "Astuce"

    👉 **Rappel**:  
    On rappelle que pour un entier `n` positif, `n % 26` renvoie le reste dans la division de `n` par 26.  
    Voici quelques exemples :  

    ```pycon
    >>> 10 % 26
    10
    >>> 26 % 26
    0
    >>> 27 % 26
    1
    >>> 28 % 26
    2
    >>> 53 % 26
    1
    >>> 
    ```  

???+ question "Compléter ci-dessous"

    {{ IDE('exo') }}


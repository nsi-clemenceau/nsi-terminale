def carre(x):
  return x ** 2

nombres = (1, 2, 3, 4, 5)
resultat = map(carre, nombres)

print(resultat)  # Un objet itérable
print(type(resultat))
print(list(resultat))


def appartient(arbre, valeur):
    if est_vide(arbre):
        return False
    elif racine(arbre) == valeur:
        return True
    return appartient(gauche(arbre), valeur) or appartient(droite(arbre), valeur)
    
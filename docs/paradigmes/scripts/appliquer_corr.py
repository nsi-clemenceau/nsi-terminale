def applique(f, liste):
    if est_vide(liste):
        return creer_liste()
    else:
        return cons(f(tete(liste)), applique(f, queue(liste)))

def incremente_1(k):
    global n
    n = n + k
    return n

def incremente_2(n, k):
    n = n + k
    return n

n = 2

for i in range(3):
    print ("L'appel incremente_1(3) donne : ", incremente_1(3))

for i in range(3):
    print ("L'appel incremente_2(2, 3) donne : ", incremente_2(2, 3))

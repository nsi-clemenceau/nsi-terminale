Solution en écriture fonctionnelle : 

```python
contient = lambda liste, v: False if est_vide(liste) else True if v == tete(liste) else contient(queue(liste), v)
```
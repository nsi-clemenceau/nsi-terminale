# Tests :
assert longueur(creer_liste()) == 0
assert longueur(cons(4, cons(3, cons(2, cons(1, creer_liste()))))) == 4

# Autres tests
assert longueur(cons(5, cons(4, cons(3, cons(2, cons(1, creer_liste())))))) == 5

😊 On peut aussi créer le dictionnaire en compréhension :

```python
def cree_lettre_nombre():
    return {chr(i + 97): i for i in range(26)}
```

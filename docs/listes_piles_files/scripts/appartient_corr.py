def appartient(x, liste):
    """
    Cette fonction retourne True si x appartient à liste, et False sinon
    Précondition : x est de n'importe quel type, liste est du type abstrait LISTE
    Postcondition : Cette fonction renvoie un booléen
    Exemples :

    >>> liste_1 = Vide()
    >>> liste_1 = Liste(1, liste_1)
    >>> liste_1 = Liste(2, liste_1)
    >>> liste_1 = Liste(3, liste_1)
    >>> appartient(4, liste_1)
    False
    >>> appartient(3, liste_1)
    True
    >>> liste_vide = Vide()
    >>> appartient(2, liste_vide)
    False

    """

    if est_vide(liste):
        return False
    elif x == tete(liste):
        return True
    else:
        return appartient(x, queue(liste))


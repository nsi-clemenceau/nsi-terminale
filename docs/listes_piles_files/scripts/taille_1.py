def taille_essai(pile):
    resultat = 0
    while not pile.est_vide_pile():
        pile.depiler()
        resultat = resultat + 1
    return resultat


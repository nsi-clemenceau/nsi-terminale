def verification(expr_math):
    """
    Cette fonction renvoie True si une expression mathématique a autant de
    parenthèses ouvrantes que de parenthèses fermantes.
    Précondition : expr_math est de type chaîne de caractères.
    Postcondition : la valeur renvoyée est de type booléen.
    exemples :

    >>> verification("(3*(2-5x)+3)")
    True
    >>> verification("(3*(2-5x)+3")
    False
    >>> verification("")
    True

    """
    p = Pile()
    for car in expr_math:
        if car == '(':
            p.empiler(car)
        if car == ')':
            if p.est_vide_pile():
                return False
            else:
                p.depiler()
    return p.est_vide_pile()




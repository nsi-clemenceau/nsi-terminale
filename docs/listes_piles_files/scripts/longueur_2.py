def longueur_rec(liste):
    """
    Cette fonction renvoie la longueur de la liste de type LISTE
    Précondition : liste est du type abstrait liste
    Postcondition : Cette fonction renvoie un entier
    Exemple :
    >>> liste_1 = Vide()
    >>> liste_2 = Liste(1, liste_1)
    >>> liste_2 = Liste(2, liste_2)
    >>> longueur_rec(liste_1)
    0
    >>> longueur_rec(liste_2)
    2

    """
    ...


# Tests
liste_1 = Vide()
liste_2 = Liste(1, liste_1)
liste_2 = Liste(2, liste_2)
assert longueur_rec(liste_1) == 0
assert longueur_rec(liste_2) == 2

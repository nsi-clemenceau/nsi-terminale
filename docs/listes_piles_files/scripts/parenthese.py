def verification(expr_math):
    """
    Cette fonction renvoie True si une expression mathématique a autant de
    parenthèses ouvrantes que de parenthèses fermantes.
    Précondition : expr_math est de type chaîne de caractères.
    Postcondition : la valeur renvoyée est de type booléen.
    exemples :

    >>> verification("(3*(2-5x)+3)")
    True
    >>> verification("(3*(2-5x)+3")
    False
    >>> verification("")
    True

    """
    ...


# Tests
assert verification("(3)*(2-5x)+3)") == False
assert verification("(3)*(2-5x)+3") == True




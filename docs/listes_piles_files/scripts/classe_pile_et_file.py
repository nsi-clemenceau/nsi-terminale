class Cellule :
    def __init__(self, contenu, suivante):
        self.contenu = contenu
        self.suivante = suivante

class Pile:
    def __init__(self):
        self.data = None

    def est_vide(self):
        return self.data == None

    def empile(self, x):
        self.data = Cellule(x, self.data)

    def depile(self):
        v = self.data.contenu  # on récupère la valeur à renvoyer
        self.data = self.data.suivante  # on supprime la 1ère cellule
        return v

    def __str__(self):
        """ Le sommet de la pile est à gauche"""
        s = "sommet -> "
        c = self.data
        while c != None :
            s += str(c.contenu)+"|"
            c = c.suivante
        return s


class File:
    def __init__(self):
        self.entree = Pile()  # pile d'entrée
        self.sortie = Pile()  # pile de sortie

    def est_vide(self):
        return self.entree.est_vide() and self.sortie.est_vide()

    def enfile(self, x):
        ...

    def defile(self):
        if self.est_vide():
            print("File vide !")
            return None

        if self.sortie.est_vide():
            while not self.entree.est_vide():
                ...

        return ...


# Tests

f = File()

for i in range(3):
    print("On enfile : ", i)
    f.enfile(i)

for i in range(3):
    print("On défile : ", f.defile())




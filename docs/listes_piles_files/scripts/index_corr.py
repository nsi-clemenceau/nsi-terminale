def lire_index(n,liste):
    """
    Cette fonction retourne l'élément de rang n de liste.
    On utilise les conventions habituelles : le plus a gauche est de rang 0,
    le suivant de rang 1 etc...
    Si n est plus grand que longueur(liste)-1, ou negatif, la fonction affiche le message : n hors limite et retourne None.
    Precondition : n est de type entier, liste est de type abstrait LISTE
    Postcondition : le type retourne est celui de l element de rang n.La fonction retourne None si n est hors limite ou si
    la liste est vide. Elle affiche alors un message explicatif.

    Exemples :

    >>> liste_1 = Vide()
    >>> liste_1 = Liste(1, liste_1)
    >>> liste_1 = Liste(2, liste_1)
    >>> liste_1 = Liste(3, liste_1)
    >>> lire_index(1, liste_1)
    2
    >>> lire_index(3, liste_1)
    n hors limite
    >>> lire_index(4, liste_1)
    n hors limite
    >>> lire_index(0, liste_1)
    3
    >>> lire_index(-1, liste_1)
    n hors limite
    >>> liste_2 = Vide()
    >>> lire_index(2, liste_2)
    liste vide

    """

    if est_vide(liste) :
        print("liste vide")
        return None
    elif n > longueur(liste) - 1 or n < 0:
        print("n hors limite")
        return None
    elif n == 0:
        return tete(liste)
    else :
        return lire_index(n - 1, queue(liste))


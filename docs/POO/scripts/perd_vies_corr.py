class Personnage_6:
    def __init__(self, nbre_vies, age):
        self.vie = nbre_vies
        self.age = age

    def donne_etat(self):
        return self.vie

    def perd_vies(self, vies_perdues):
        self.vie = self.vie - vies_perdues

    def boire_potion(self):
        self.vie = self.vie + 1

gollum = Personnage_6(15, 100)
gollum.perd_vies(2)
print(gollum.donne_etat())




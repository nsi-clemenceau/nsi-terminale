from random import randint

class Personnage_9:
    def __init__(self, nbre_vies, age):
        self.vie = nbre_vies
        self.age = age

    def donne_etat(self):
        return self.vie

    def perd_vies(self, vies_perdues):
        perdues = randint(1, vies_perdues)
        self.vie = self.vie - perdues
        if self.vie < 0:
            self.vie = 0

    def boire_potion(self):
        self.vie = self.vie + 1

gollum = Personnage_9(100, 127)
bilbo = Personnage_9(100, 127)
nb_attaques = 0
attaquant = randint(0, 1)
while gollum.donne_etat() > 0 and bilbo.donne_etat() > 0 :
    if attaquant == 0 :
        gollum.perd_vies(10)
    else :
        bilbo.perd_vies(10)
    nb_attaques = nb_attaques + 1
    attaquant = 1 - attaquant
print(nb_attaques)
print("Gollum a ", gollum.donne_etat(), "vies")
print("bilbo a ", bilbo.donne_etat(), "vies")

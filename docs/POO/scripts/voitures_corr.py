class Voiture:

    def __init__(self, nom, couleur, puissance):
        self.nom = nom
        self.couleur = couleur
        self.puissance = puissance

    def nom_puissance(self):
        return self.nom + " - puissance : " + str(self.puissance)

voiture_1 = Voiture("Coccinelle", "rouge", 5)
print(voiture_1.nom_puissance())
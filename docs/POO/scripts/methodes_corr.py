class Personnage_4:
    def __init__(self, nbre_vies, age):
        self.vie = nbre_vies
        self.age = age

    def donne_etat(self):
        return self.vie

    def perd_une_vie(self):
        self.vie = self.vie - 1

gollum = Personnage_4(20, 127)
etat_1 = gollum.donne_etat()
print(etat_1)
gollum.perd_une_vie()
etat_2 = gollum.donne_etat()
print(etat_2)



---
author: Jean-Louis Thirot, Nicolas Revéret et Mireille Coilhac
title: Diviser/régner
---
<p style="padding:5px;
          background-color:#afa;
          border:1px solid #bbffbb;
          border-radius:20px;
          font-weight:bold;
          font-size:2em;
          padding:15px;
          text-align:center;">

    Diviser pour régner

</p><br> 

## I. Présentation

[Vidéo Lumni](http://www.lumni.fr/video/la-methode-laquo-diviser-pour-regner-raquo){ .md-button target="_blank" rel="noopener" }


??? note "Diviser"

    👉 Découper un problème initial en sous problèmes.


??? note "Régner"

    👉 Résoudre les sous-problèmes (récursivement ou directement s'ils sont assez petits)


??? note "Combiner"

    👉 Trouver une solution au problème initial à partir des solutions des sous-problèmes.

## II. Premiers exemples : 

Quelques exemples d'utilisation de "diviser pour régner" :

<div class="centre" markdown="span">
<iframe 
src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/diviser_regner_2022_sujet.ipynb"
width="900" height="1000" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>

La correction est arrivée 😊

<div class="centre" markdown="span">
<iframe 
src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/diviser_regner_2023_corr.ipynb"
width="900" height="1000" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>
    
<!--- La correction 

⏳ La correction viendra bientôt ... 

La correction est arrivée 😊

<div class="centre" markdown="span">
<iframe 
src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/diviser_regner_2023_corr.ipynb"
width="900" height="1000" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>

-->



## III. Une approche de la complexité

[Vidéo de Cédric Gerland](https://youtube.com/watch?v=UcT_4cWfnAs&si=EnSIkaIECMiOmarE){ .md-button target="_blank" rel="noopener" }

https://youtube.com/watch?v=UcT_4cWfnAs&si=EnSIkaIECMiOmarE





## IV. Le tri fusion

###  🤔 Comment fusionner deux listes triées pour obtenir une liste triée ?

Nous donnons `l1 = [2, 3, 5, 8]` et `l2 = [1, 4]`  

#### ✏️ A vos crayons 1 :

Voici un script Python :

```python
def mystere(l1, l2):
    n1 = len(l1)
    n2 = len(l2)
    lst = [] # initialisation de la fusion de l1 et l2 
    i1 = 0 # indice qui sert à parcourir l1
    i2 = 0 # indice qui sert à parcourir l2
    while i1 < n1 and i2 < n2 :
        if l1[i1] < l2[i2]:
            lst.append(l1[i1])
            i1 = i1 + 1
        else :
            lst.append(l2[i2])
            i2 = i2 + 1
    return lst

mystere([2, 3, 5, 8], [1, 4])   
```

Recopier sur **votre cahier** le tableau suivant qui décrit le déroulement de l'exécution de :   
`mystere([2, 3, 5, 8],[1, 4])`  et le compléter.

* Il y a une ligne par tour de boucle.  
* Pour vous aider, nous avons rajouté une colonne pour `l1` et une pour `l2`. Vous pourrez entourer à chaque étape, dans une de ces colonnes, l'élément qui sera ajouté à `lst`  (en gras ici)

|``i1`` |``i2`` | ``l1   ``            | ``l2 ``       | ``lst ``             |
|:--    |:--    |:--                   |:--            |:--                   |
|0      |0      | [2, 3, 5, 8]         | [**1**, 4]    | [1]                  |
|0      |1      | [**2**, 3, 5, 8]     | [**1**, 4]    | [1, 2]               |
|...    |...    | ...                  | ...           | ...                  |
...


??? success "Solution"

    |``i1`` |``i2`` | ``l1   ``            | ``l2 ``       | ``lst ``             |
    |:--    |:--    |:--                   |:--            |:--                   |
    |0      |0      | [2, 3, 5, 8]         | [**1**, 4]    | [1]                  |
    |0      |1      | [**2**, 3, 5, 8]     | [**1**, 4]    | [1, 2]               |
    |1      |1      | [**2**, **3**, 5, 8]     | [**1**, 4]    | [1, 2, 3]         |
    |2      |1      | [**2**, **3**, 5, 8]     | [**1**, **4**]    | [1, 2, 3, 4]  |
    |2      |2      |        |  |


😥 Nous observons que les deux listes n'ont pas été complètement fusionnées, car nous avons "épuisé" tous les éléments de `l2`.  
👉 Par contre, il est certain que les éléments restants de `l1` qui n'ont pas été fusionnés, sont triés, et plus grands que tous les éléments déjà présents dans `lst`.  
🤗 Pour obtenir la liste complètement fusionnée, il suffit donc d'exécuter :  
`lst + [5, 8]` c'est à dire `lst + l1[i1:]`

???+question "💻 A vous de jouer 1"

    Compléter le script suivant :

    {{IDE('scripts/fusion')}}


😥 Le problème, c'est que les "slices" ne sont pas vraiment au programme de NSI, et que leur utilisation n'est pas toujours efficace. Essayons une version qui n'utilise pas de "slices".   

???+question "💻 A vous de jouer 2"

    Compléter le script suivant :

    {{IDE('scripts/fusion_2')}}

### Cours détaillé

???+ note dépliée "Cours de Nicolas Revéret"

    Dans ce cours les tableaux sont des tableaux de taille fixe.
    La syntaxe `.append` n'est donc jamais utilisée.

    ⏳ Nous étudierons une autre présentation de ce tri avec le type `list` Python dans un second temps

    [Cours de Nicolas Revéret](https://e-nsi.forge.aeif.fr/tris/04_rapides/01_fusion/){ .md-button target="_blank" rel="noopener" }


### ➗ Diviser Pour Régner : le tri fusion 🤴

   
😊 Nous allons maintenant implémenter une méthode de tri basée sur "diviser pour régner" : le tri fusion.

Observons  cette animation : 

[Illustration animée (source wikipédia)](https://upload.wikimedia.org/wikipedia/commons/c/cc/Merge-sort-example-300px.gif)  


Nous disposons d'un tableau (type `list` de Python) de taille ``n``.  
Son premier rang est donc ``0`` et son dernier rang ``n-1``.  
On notera ``t[a -> b]`` la liste constituée des éléments de rang compris entre ``a`` et ``b`` (compris) de la liste ``t``.  
La fonction ``tri_fusion`` fait appel à la fonction ``fusion`` qui permet de fusionner deux listes triées en une liste triée.  


<pre>

fonction tri_fusion(t)
      """
      entrée ː un tableau t
      sortie ː renvoie un autre tableau qui correspond au tableau t trié
      """
      n = longueur(t)
      si n ≤ 1
              renvoyer t
      sinon 
              m = n//2
              renvoyer fusion(tri_fusion(t[0 -> m-1]), tri_fusion(t[m -> n-1])) 
              
 </pre>
  
La terminaison est justifiée par la décroissance stricte de `n` à chaque appel récursif.  

???+question "💻 A vous de jouer 3"

    Compléter le script suivant :

    {{IDE('scripts/tri_fusion_listes')}}


<!--- La correction à télécharger plus tard

⏳ La correction viendra bientôt ...

<div class="centre" markdown="span">
<iframe 
src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/td_tri_fusion_2022_corrige.ipynb"
width="900" height="1000" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>
-->

### Une approche de la complexité du tri fusion

    
<div class="centre" markdown="span">
<iframe 
src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/td_compl_tri_fusion_2023_sujet.ipynb"
width="900" height="1000" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>


⏳ La correction viendra bientôt ... 

<!--- La correction à télécharger plus tard

⏳ La correction viendra bientôt ... 

<div class="centre" markdown="span">
<iframe 
src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/td_compl_tri_fusion_2023_corr.ipynb"
width="900" height="1000" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>

-->
    

### Une vidéo de Cédric Gerland sur le tri fusion et sa complexité

<center>
<iframe width="635" height="357" src="https://www.youtube.com/embed/OEmlVnH3aUg"  frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>



!!! abstract "Complexité"

    Le tri fusion a un coût en $n \log_2 n$


### Bilan

!!! abstract "Le tri fusion en entier"

    ```python
    def fusion(l1, l2):
        """
        Précondition : l1 et l2 sont deux listes triées
        Postcondition : la fonction renvoie une liste triée constituée de la fusion 
        de l1 et l2
        Exemple :
        fusion([2, 3, 5, 8],[1, 4]) renvoie [1, 2, 3, 4, 5, 8]
        """
        n1 = len(l1)
        n2 = len(l2)
        lst = [] # initialisation de la fusion de l1 et l2 
        i1 = 0 # indice qui sert à parcourir l1
        i2 = 0 # indice qui sert à parcourir l2
        while i1 < n1 and i2 < n2 :
            if l1[i1] < l2[i2]:
                lst.append(l1[i1])
                i1 = i1 + 1
            else :
                lst.append(l2[i2])
                i2 = i2 + 1
        if i1 == n1:
            return lst + l2[i2:]
        if i2 == n2:
            return lst + l1[i1:]
    
    def tri_fusion(lst):
        """
        Précondition : lst est une liste
        Postcondition : la fonction renvoie une liste qui est la liste triée
        """
        n = len(lst)
        if n <= 1:
            return lst
        else :
            m = n // 2
            return fusion(tri_fusion(lst[:m]), tri_fusion(lst[m:])) 
    ```
    



